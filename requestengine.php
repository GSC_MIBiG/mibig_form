<?php

$EmailFrom = Trim(stripslashes($_POST['Email'])); 
//$EmailFrom = "antismash@biotech.uni-tuebingen.de"; 
$EmailTo = "marnix.medema@wur.nl";
//$EmailTo = "kai.blin@biotech.uni-tuebingen.de";
$Subject = "MIBiG update / request";
$Name = Trim(stripslashes($_POST['Name'])); 
$Email = Trim(stripslashes($_POST['Email'])); 
$Compound = Trim(stripslashes($_POST['Compound'])); 
$myInputs = $_POST['myInputs'];
$myStartCoords = $_POST['myStartCoords']; 
$myEndCoords = $_POST['myEndCoords']; 

// validation
$validationOK=true;
if (!$validationOK) {
  print "<meta http-equiv=\"refresh\" content=\"0;URL=error.htm\">";
  exit;
}

$Body = "";
$Body .= "Name: ";
$Body .= $Name;
$Body .= "\n";
$Body .= "Email: ";
$Body .= $Email;
$Body .= "\n";
$Body .= "Compound: ";
$Body .= $Compound;
$Body .= "\n";
$Body .= "Loci: \n";
$i = 0;
foreach ($myInputs as $eachInput) {
     $Body .= $eachInput . " (" . $myStartCoords[$i] . "-" . $myEndCoords[$i] . ")\n";
     $i++;
}
$Body .= "\n";

// send email 
$success = mail($EmailTo, $Subject, $Body, "From: <$EmailFrom>");

// redirect to success page 
if ($success){
  print "<meta http-equiv=\"refresh\" content=\"0;URL=contactthanks.php\">";
}
else{
  print "<meta http-equiv=\"refresh\" content=\"0;URL=error.html\">";
}
?>
