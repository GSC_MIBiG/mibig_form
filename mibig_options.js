var mibig_options = {
  "renderForm": true,
  "form": {
      "toggleSubmitValidState": false,
      "attributes": {
          "id": "mibig-form",
          "method": "post",
          "action": "http://..."
      },
      //"buttons": {
      //    "submit": {},
      //    "reset": {}
      //}
  },
  "fields" : {
    "personal":{
      "fields": {
        "submitter_name": {
          "type":"personalname"
        }
      }
    },
    "general_params":{
      "fields":{
    "biosyn_class" : {
      "vertical": "true",
      "type": "select",
      "multiple": true,
      "size": 7
    },
    "loci":{
      "collapsed": true,
      "fields":{
        "nucl_acc": {
          "itemLabel": "Genomic locus",
          "toolbarSticky": true
        }
      }
    },
        
    "compounds" : {
      "itemLabel": "Compound",
      "toolbarSticky": true,
      "type": "map",
      "collapsed": true,
      "fields": {
        "item":{
          "fields":{
            "chem_synonyms":{
              "type": "tag"
            },
            "evidence_struct":{
              "type": "select",
              "multiple": true,
              "size": 6,
            },
            "chem_act":{
              "type": "select",
              "multiple": true,
              "size": 10,
            },
            "other_chem_act" : {
              "dependencies" : {
                "chem_act": "Other"
              }, 
            },
            "chem_moieties" : {
              "fields":{
                "other_chem_moiety": {
                  "dependencies": {
                    "chem_moiety": "Other"
                  }
                },
                "moiety_subcluster": {
                  "type": "tag"
                }
              }
            }
          }
        }
      }
    },
    "genes" : {
      "collapsed": true,
      "fields":{
        "gene": {
          "itemLabel": "Gene",
          "type": "map",
          "toolbarSticky": true,
          "fields": {
            "item":{
              "fields":{
                "tailoring":{
                  "dependencies":{
                    "gene_function":"Tailoring"
                  }
                },
                "evidence_code" : {
                  "dependencies" : {
                    "gene_function":['Unknown'] ///Should be negative dependency, currently works because of brackets
                  }, 
                }
              }
            }
          }
        },
        "operon": {
          "itemLabel": "Operon",
          "toolbarSticky": true,
          "fields": {
            "item":{
              "fields":{
              "operon_genes": {
                "type": "tag"
              }
              }
            }
          }
        }
      }
    },
    "NRP" : {
      "dependencies":{
        "biosyn_class":"NRP"
      },
      "collapsed": true,
      "fields" : {
        //"naa" : {
        //  "type" : "tag",
        //  "label" : "NRP amino acid sequence"
        //}
        "nrps_genes": {
          "toolbarSticky": true,
          "itemLabel": "NRPS gene",
          "type": "map",
          "fields":{
              "item" : {
                "fields":{
                  "nrps_module": {
                    "toolbarSticky": true, 
                    "fields":{
                      "item" : {
                        "fields":{
                          "substr_spec":{
                            "fields":{
                            "prot_adom_spec": {
                              "dependencies": {
                                "aa_type":"Proteinogenic"
                              }
                            },
                            "nonprot_adom_spec": {
                              "dependencies": {
                                "aa_type":"Nonproteinogenic"
                              }
                            },
                            "other_spec": {
                              "dependencies": {
                                "nonprot_adom_spec":"Other"
                              }
                            },
                            "multiple_spec": {
                              "dependencies": {
                                "aa_type":"Multiple (promiscuous)"
                              }
                            },
                            "epimerized": {
                              "type": "checkbox",
                              "rightLabel": "Epimerized to D-enantiomer by epimerization domain or epimerase."
                            },
                            "subcluster": {
                              "type": "tag",
                              "dependencies": {
                                "aa_type":"Nonproteinogenic"
                              }
                            }
                            }
                          },
                          "other_mod_dom": {
                            "dependencies": {
                              "mod_doms": "Other"
                            }
                        },
                          "evidence_skip_iter": {
                            "dependencies": {
                              "mod_skip_iter": ["Neither"]
                            }
                        }
                        }
                      }
                    }
                  }
                }
              }
          }
        },
        "thioesterase" : {
          "type": "tag",
          "dependencies":{
            "te_type": ['Type I', 'Unknown', 'None']
          },
        },
      }
    },
    "Polyketide" : {
      "dependencies":{
        "biosyn_class":"Polyketide"
      },
      "collapsed": true,
      "fields" : {
        "starter_unit" : {
          //"dependencies":{
          //  "pks_subclass": ["Enediyne type I", "Type II", "Type III", "Other"]
          //},
        },
        "pks_subclass": {
            "type": "select",
            "multiple": true,
            "vertical": "true",
            "size": 7
        },
        "mod_pks_genes": {
          "dependencies":{
            "pks_subclass": ["Iterative type I", "Enediyne type I", "Type II", "Type III", "Other"],
          },
          "toolbarSticky": true,
          "itemLabel": "PKS gene",
          "type": "map",
          "fields":{
              "item" : {
                "fields":{
                  "pks_module": {
                    "toolbarSticky": true, 
                    "fields":{
                      "item" : {
                        "fields":{
                          "pks_domains" : {
                              "vertical": "true",
                              "type": "select",
                              "multiple": true,
                              "size": 6
                          },
                          "at_substr_spec" : {
                              "dependencies": {
                                "pks_domains": ['Ketoreductase', 'Dehydratase', 'Enoylreductase']
                              }
                          },
                          "evidence_at_spec" : {
                              "dependencies": {
                                "pks_domains": ['Ketoreductase', 'Dehydratase', 'Enoylreductase'],
                                "at_substr_spec": ["Unknown"]
                              }
                          },
                          "multiple_spec": {
                            "dependencies": {
                              "at_substr_spec": "Multiple (promiscuous)"
                            }
                          },
                          "kr_stereochem" : {
                              "dependencies": {
                                "pks_domains": "Ketoreductase"
                              }
                          },
                          "other_mod_dom" : {
                              "dependencies": {
                                "mod_doms": "Other"
                              }
                          },
                          "evidence_skip_iter" : {
                              "dependencies": {
                                "mod_skip_iter": ["Neither"]
                              }
                          }
                        }
                      }
                    }
                  }
                }
              }
          }
        },
        "pks_genes" : {
          "type": "tag",
          "dependencies":{
            "pks_subclass": ["Modular type I", "Trans-AT type I"]
          },
        },
        "nr_iterations" : {
          "dependencies":{
            "pks_subclass": "Iterative type I"
          },
        },
        "iterative_subtype" : {
          "dependencies":{
            "pks_subclass": "Iterative type I"
          },
        },
        "iter_cycl_type" : {
          "dependencies":{
            "pks_subclass": "Iterative type I"
          },
        },
        "trans_at" : {
          "type": "tag",
          "dependencies":{
            "pks_subclass": "Trans-AT type I"
          },
        },
        "ketide_length" : {
          "dependencies":{
            "pks_subclass": ["Modular type I", "Trans-AT type I", "Iterative type I", "Enediyne type I", "Other"]
          },
        },
        "cyclases" : {
          "dependencies":{
            "pks_subclass": "Type II"
          },
        },
        "te_type" : {
          "dependencies":{
            "pks_subclass": ["Iterative type I", "Type II", "Type III", "Other"]
          },
        },
        "thioesterase" : {
          "type": "tag",
          "dependencies":{
            "pks_subclass": ["Iterative type I", "Enediyne type I", "Type II", "Type III", "Other"],
            "te_type": ['Type I', 'Unknown', 'None'] //ISSUE HERE WITH NESTED DEPENDENCIES: FIELD STAYS IF PKS_SUBCLASS IS CHANGED
          },
        },
        "release_type" : {
          "dependencies":{
            "pks_subclass": ["Iterative type I", "Type II", "Type III", "Other"]
          },
        }
      }
    },
    "RiPP" : {
      "dependencies":{
        "biosyn_class":"RiPP"
      },
      "collapsed": true,
      "fields" : {
        "precursor_loci": {
          "fields": {
            "item": {
              "fields": {
                "gene_id" : {
                  "type" : "tag",
                },
                "peptidase" : {
                  "type" : "tag",
                }
              }
            }
          }
        }
      }
    },
    "Terpene" : {
      "dependencies":{
        "biosyn_class":"Terpene"
      },
      "collapsed": true,
      "fields" : {
        "terpene_synth_cycl": {
          "type": "tag"
        },
        "prenyl_transf": {
          "type": "tag"
        }
      }
    },
    "Saccharide" : {
      "dependencies":{
        "biosyn_class":"Saccharide"
      },
      "collapsed": true,
      "fields" : {
        "gt_genes" : {
          "toolbarSticky": true,
          "type": "map",
          "fields":{
              "item" : {
                "fields":{
                    "evidence_gt_spec" : {
                      "dependencies" : {
                        "gt_specificity":['Unknown'] ///Should be negative dependency, currently works
                      }, 
                    },
                    "other_gt_spec" : {
                      "dependencies" : {
                        "gt_specificity":'Other'
                      },
                    },
                    "sugar_subcluster" : {
                      "dependencies" : {
                        "gt_specificity":['Unknown'] ///Should be negative dependency, currently works
                      }, 
                    },
                }
              }
          },
        },
      },
    },
    "Alkaloid" : {
      "dependencies":{
        "biosyn_class":"Alkaloid"
      },
      "collapsed": true
    },
    "Other" : {
      "dependencies":{
        "biosyn_class":"Other"
      },
      "collapsed": true
    }
    }},
    "comments": {
      "type": "textarea",
      "rows": 6,
      "cols": 80
    }
  },
};


var mibig_schema = {
  "title": "MIBiG Submission form",
  "description": "Please fill out this form to submit MIBiG-compliant annotation data on a biosynthetic gene cluster.",
  "type": "object",
  "properties": {
    "personal":{
    "type": "object",
    "title": "Personal data",
    "properties": {
        "submitter_name": {
        "type": "string",
        "title": "Your name",
        "required": true
        },
        "submitter_institution": {
        "type":"string",
        "title": "Your institution",
        "description": "Please use the full, official name of your institute in English.",
        "format": "string",
        "required": true
        },
        "submitter_email": {
        "type":"string",
        "title": "Your e-mail address",
        "format": "email",
        "required": true
        },
    },
    },
    "general_params":{
    "type": "object",
    "title": "MIBiG data",
    "properties": {

    "biosyn_class": {
      "type":"string",
      "title": "Biosynthetic class",
      "required": true,
      "enum": ['NRP', 'Polyketide', 'RiPP', 'Terpene', 'Saccharide', 'Alkaloid', 'Other'],
      "description": "Hold ctrl or cmd key to select multiple classes in case of a hybrid gene cluster"
    },
    "publications": { //MOVE UP
      "type":"string",
      "pattern": "[0-9]{1,}",
      "title": "Key publications associated with this gene cluster, pathway or molecule",
      "description" : "Publications describing experiments on this pathway. Please input PubMed IDs, separated by comma's."
    },
    "loci":{
      "type": "object",
      "title": "Gene cluster sequence information",
      "description": "Specify the gene cluster sequence as deposited/submitted in/to GenBank (or ENA/DDBJ). If the pathway is split over multiple clusters, add multiple genomic locus entries below. The genome or gene cluster sequence must be submitted to GenBank/ENA/DDBJ (and an accession number must be received) before this form can be filled out.",
      "properties": {
        "complete": {
          "title": "Complete gene cluster sequence?",
          "type": "string",
          "enum": ['complete','incomplete', 'unknown'],
          "required": true,
          "default": "None"
        },
        "nucl_acc": {
          "description": "Genomic loci",
          "type": "array",
          "required": true,
          "items": {
              "type": "object",
              "title": "Genomic locus",
              "properties": {
                  "Accession": {
                  "title": "GenBank nucleotide accession number",
                  "type": "string",
                  "description": "E.g., AL645882. Only use GenBank accessions, not RefSeq accessions or GI numbers.",
                  "required": true,
                  },
                  "Start coordinate" : {
                  "title": "Start coordinate",
                  "type": "integer",
                  "description":"May be left empty if gene cluster covers entire GenBank nucleotide record."
                  },
                  "End coordinate" : {
                  "title": "End coordinate",
                  "type": "integer",
                  "description":"May be left empty if gene cluster covers entire GenBank nucleotide record."
                  },
                  "conn_comp_cluster" : {
                  "title": "Evidence of involvement",
                  "type": "string",
                  "required": true,
                  "enum": ['Knock-out studies','Expression studies','Enzymatic assays', 'Sequence-based prediction','Other']
                  },
              }
          }
        },
        "ribo_marker": {
          "type":"string",
          "title": "Host organism 16S marker gene",
          "description": "Optional. 16S rRNA nucleotide sequence of strain of origin. Only include the 16S of the strain that harbors the gene cluster that is submitted, not of other strains that carry homologues of the gene cluster."
        },
      }
    },
    "compounds":{
      "type": "array",
      "title": "Chemical compound(s)",
      "description": "Add one entry for each molecule produced from this pathway / gene cluster.",
      "items": {
          "type": "object",
          "title": "Chemical compound",
          "properties": {
              "_key": {
              "type":"string",
              "title": "Compound name",
              "required": true,
              },
              "chem_synonyms" : {
              "title": "Synonyms for this compound",
              "description": "Synonyms for the compound, separated by comma's.",
              "type": "string",
              },
              "chem_struct": {
              "type":"string",
              "title": "Compound structure",
              "pattern":"^[0123456789BCOHNSOPMgrIFKZAsnlauioe@%+\\-\\[\\]\\(\\)\\\\/=#$]{6,}$",  
              "description": "Chemical structure entered as SMILES string, preferentially isomeric",
              "required": true,
              },
              "pubchem_id": {
              "type":"integer",
              "title": "PubChem ID, if available",
              "description": "If no PubChem or ChEMBL entry available, we encourage submission to PubChem, ChEMBL or ChemSpider.",
              },
              "chembl_id": {
              "type":"integer",
              "title": "ChEMBL ID, if available",
              "description": "If no PubChem or ChEMBL entry available, we encourage submission to PubChem, ChEMBL or ChemSpider.",
              },
              "evidence_struct" : {
              "title": "Technique(s) used to elucidate structure",
              "description": "Hold ctrl or cmd key to select multiple items",
              "type": "string",
              "required": true,
              "enum": ['NMR', 'Mass spectrometry', 'X-ray', 'Chemical derivatization', 'Total synthesis', 'Other']
              },
              "mol_mass" : {
              "title": "Exact molecular mass",
              "description": "Exact molecular mass in Daltons. Use a dot as a decimal point, not a comma.",
              "required": true,
              "type": "number"
              },
              "molecular_formula" : {
              "title": "Molecular formula",
              "type": "string"
              },
              "chem_act" : {
              "title": "Molecular activities",
              "type": "string",
              "required": true,
              "enum": ['Unknown', 'Antibacterial', 'Antifungal', 'Cytotoxic', 'Siderophore', 'Inhibitor', 'Surfactant', 'Signalling', 'Antioxidant', 'Other'],
              "description":"Hold ctrl or cmd key to select multiple items. Enter only proven activities; always provide the publication IDs where the evidence was provided under 'Key publications' below.",
              },
              "other_chem_act" : {
              "title": "Other activity",
              "type": "string",
              "dependencies": "chem_act"
              },
              "chem_target" : {
              "title": "Molecular targets",
              "type": "string",
              "description": "Enter proteins, RNAs or other (macro)molecules targeted by this compound, separated by comma's. Enter only proven targets; always provide the publication IDs where the evidence was provided under 'Key publications' below."
              },
              "chem_moieties" : {
              "title": "Unusual chemical moieties in this molecule",
              "type": "array",
              "description":"With 'unusual', it is meant that they are not covered by NRPS/PKS/RiPP/terpene/saccharide biosynthetic mechanisms.",
              "items": {
                "type": "object",
                "title": "Chemical moiety",
                "properties": {
                  "chem_moiety" : {
                  "title": "Chemical moiety",
                  "type": "string",
                  "enum": ['3-Amino-5-hydroxybenzoic acid', '3-Dimethylallyl-4-hydroxybenzoic_acid', '6-methylsalicylic acid', 'Aminocoumarin', 'Benzoic acid', 'Indole group', 'Lipid', 'Orsellinic acid', 'Pyrrole ring', 'Other'],
                  },
                  "other_chem_moiety" : {
                  "title": "Specify other chemical moiety",
                  "dependencies": "chem_moiety",
                  "type": "string"
                  },
                  "subcluster" : {
                  "title": "Subcluster involved in the biosynthesis of this moiety",
                  "type": "string",
                  "description": "Enter locus tags, protein IDs or gene IDs (in this order of preference) exactly as they are specified in the submitted GenBank/ENA/DDBJ entry, separated by comma's."
                  },
                }
              }
              },
          },
      },
    },       
    "genes":{
      "type": "object",
      "title": "Genes in this gene cluster",
      "description": "Add custom gene names or annotations of experimentally verified functional or expression data for those genes for which this is available. You do not need to create items for genes on which no experimental data are available. These will be detected in the GenBank entry and added to the visualization automatically.",
      "properties": {
          "gene": {
          "title": "Genes",
          "type": "array",
          "items": {
              "type": "object",
              "title": "Gene",
              "properties": {
                  "_key": {
                  "type":"string",
                  "title": "Gene name",
                  "description": "Custom gene name. E.g., 'eryA'. Not needed if the GenBank entry already contains this information in a 'gene' tag within the corresponding CDS feature."
                  },
                  "gene_id": {
                  "type":"string",
                  "title": "Gene ID",
                  "description": "Enter locus tag, protein ID or gene ID (in this order of preference) exactly as it is specified in the submitted GenBank/ENA/DDBJ entry."
                  },
                  "gene_function" : {
                  "title": "Gene function",
                  "type": "string",
                  "default": "None",
                  "required": true,
                  "enum": ['Unknown','Scaffold biosynthesis', 'Precursor biosynthesis', 'Tailoring', 'Transport', 'Regulation', 'Resistance/immunity', 'Other'],
                  },
                  "tailoring" : {
                  "dependencies":"gene_function",
                  "title": "Tailoring reaction type",
                  "type": "string",
                  "required": true,
                  "enum": ['Unknown','Oxidation', 'Reduction', 'Methylation', 'Glycosylation', 'Amination', 'Halogenation', 'Sulfation', 'Prenylation', 'Acylation', 'Acetylation', 'Phosphorylation', 'Hydroxylation', 'Dehydration', 'Deoxygenation', 'Epimerization', 'Other'],
                  },
                  "evidence_tailoring" : {
                  "dependencies":"gene_function",
                  "title": "Evidence for function",
                  "type": "string",
                  "enum": ['Knock-out', 'Activity assay', 'Sequence-based prediction', 'Other'],
                  },
                  "mut_pheno" : {
                  "title": "Knockout mutation phenotype for this gene, if known",
                  "type": "string"
                  },
              },
          },
          },
          "operon": {
          "title":"Operons",
          "type": "array",
            "items": {
              "type": "object",
              "title": "Operon",
              "properties": {
                "operon_genes": {
                "title": "Genes in operon",
                "type": "string",
                "description": "Enter locus tags, protein IDs or gene IDs (in this order of preference) that make up this operon, exactly as they are specified in the submitted GenBank/ENA/DDBJ entry, from 5' to 3', separated by comma's."
                },
                "evidence_operon" : {
                "title": "Experimental evidence for this operon",
                "type": "string",
                "enum": ['RNAseq', 'RACE', 'ChIPseq', 'Sequence-based prediction', 'Other'],
                },
              }
            }
          }
      }
    },
    "NRP": {
      "title" : "NRP-specific parameters",
      "type": "object",
      "dependencies": "biosyn_class",
      "properties": {
        "subclass": {
          "title": "subclass",
          "type": "string",
          "required": true,
          "enum": ['glycopeptide','Ca+-dependent lipopeptide','beta-lactam','siderophore','cyclic depsipeptide','glycopeptidolipid','pyrrolobenzodiazepine','uridyl peptide', 'other']

        },
        "lin_cycl_nrp": {
          "type": "string",
          "required": true,
          "enum": ['Linear', 'Cyclic'],
          "description": "Also counts as cyclic if the molecule is hybrid and the NRP cyclizes onto another part of the hybrid molecule. Internal cyclizations of an amino acid or cyclizations of two adjacent amino acids or ketide groups do not count to define a cyclic compound: a cyclization should bridge residues that would otherwise not be connected."
        },
        "nrps_genes": {
          "title": "NRPS genes in cluster",
          "description": "Nonribosomal peptide synthetase-encoding genes; use locus_tags or protein IDs matching those in the GenBank entry",
          "type": "array",
          "items": {
            "type":"object",
              "title": "NRPS gene",
              "minItems":1,
              "properties":{
                  "_key":{
                   "title": "NRPS-encoding gene",
                   "description": "Enter locus tag, protein ID or gene ID (in this order of preference) exactly as it is specified in the submitted GenBank/ENA/DDBJ entry.",
                   "type": "string"
                  },
                  "nrps_module": {
                  "title": "NRPS modules in this gene",
                  "type": "array",
                  "items": {
                      "type": "object",
                      "title": "NRPS Module",
                      "properties": {
                          "module_nr": {
                          "title": "Assembly line module number",
                          "type": "string",
                          "pattern": "^[ABCD0123456789]{1,}$",
                          "description": "Enter the modules from N- to C-terminus in this gene. Counting starts from 1 at the start of the assembly-line (which can cover multiple genes). Every module with at least an A-domain (or AT-domain, in the case of hybrid assembly-lines) is included. For hybrids, PKS modules are included in the counting. If this NRPS module is not part of a main assembly-line for producing this compound or if the NRPS complex is too noncanonical to describe in a linear fashion, please enter '0'. If there are multiple assembly-lines involved in synthesis of the main product, these can be indicated as 'A1, A2, A3, etc.' and 'B1, B2, B3'. Similarly, if the assembly-line branches at a later stage to make multiple products, the shared part can be called e.g. '1, 2, 3' and the split part 'A4, A5' and 'B4, B5'. Up to four parallel assembly-lines are supported (with letters A-D).",
                          "required": true,
                          },
                          "substr_spec": {
                          "title": "Adenylation domain substrate specificity",
                          "type": "object",
                          "properties": {
                            "aa_type": {
                            "type": "string",
                            "description": "Enter 'None' if module is skipped or inactive.",
                            "enum": ["None", "Unknown", "Proteinogenic", "Nonproteinogenic", "Multiple (promiscuous)"],
                            "required": true,
                            },
                            "prot_adom_spec": {
                            "type": "string",
                            "required": true,
                            "enum":['Alanine', 'Arginine', 'Asparigine', 'Aspartate', 'Cysteine', 'Glutamine', 'Glutamate', 'Glycine', 'Histidine', 'Isoleucine', 'Leucine', 'Lysine', 'Methionine', 'Phenylalanine', 'Proline', 'Serine', 'Threonine', 'Tryptophan', 'Tyrosine', 'Valine'],
                            "dependencies": "aa_type"
                            },
                            "nonprot_adom_spec": {
                            "type": "string",
                            "enum": ['2-Amino-adipic-acid', '2-Aminobutyric acid', '2-Aminoisobutyric acid', '2-Hydroxy-3-methyl-pentanoic acid', '2-Hydroxyisovalerate', '2,3-Dehydro-2-aminobutyric acid', '2,3-Diaminopropionic acid', '2,3-Dihydroxybenzoic acid', '2,4-Diaminobutyric acid', '3,5-Dihydroxy-phenylglycine', '4-Hydroxyphenylglycine', '4-Hydroxyproline', 'Allo-threonine', 'Beta-alanine', 'Beta-hydroxytyrosine', 'Citrulline', 'Homoproline', 'Homoserine', 'Homotyrosine', 'Hydroxy-cycloornithine', 'Hydroxyasparagine', 'Hydroxyaspartic acid', 'Isovaline', 'Kynurenine', 'N-Acetyl-hydroxyornithine', 'N-Methyl-alanine', 'N-Methyl-Glycine', 'N-Methyl-hydroxyphenylglycine', 'N-Methyl-isoleucine', 'N-Methyl-leucine', 'N-Methyl-phenylalanine', 'N-Methyl-phenylglycine', 'N-Methyl-valine', 'N5-Hydroxyornithine', 'N6-Formyl-hydroxyornithine', 'Ornithine', 'Phenylalaninol', 'Phenylglycine', 'Pipecolic acid', 'Putrescine', 'Salicylic acid', 'Other'],
                            "required": true,
                            "dependencies": "aa_type"
                            },
                            "other_spec" : {
                            "title": "Specify other amino acid",
                            "type": "string",
                            "dependencies": "nonprot_adom_spec",
                            "description": "Use the monomer nomenclature from NORINE whenever possible."
                            },
                            "multiple_spec" : {
                            "title": "Specify promiscuous amino acid specificity",
                            "type": "string",
                            "dependencies": "aa_type",
                            "description": "Input multiple amino acids, separated by semi-colons (';'). Use the monomer nomenclature from NORINE whenever possible."
                            },
                            "epimerized": {
                            },
                            "subcluster" : {
                            "title": "Sub-cluster for amino acid biosynthesis, if within this gene cluster",
                            "type": "string",
                            "description":"Enter locus tags, protein IDs or gene IDs (in this order of preference) exactly as they are specified in the submitted GenBank/ENA/DDBJ entry, separated by comma's. A sub-cluster only needs to be declared once, in case there are multiple instances of the nonproteinogenic amino acid in the assembly-line.",
                            "dependencies": "aa_type" //OPTIONS -> MAKE TAG
                            },
                            "evidence_a_spec" : {
                            "title": "Evidence",
                            "description":"Choose 'None' if substrate specificity not known or not applicable.",
                            "type": "string",
                            "enum": ['Structure-based inference', 'Activity assay', 'Sequence-based prediction', 'Other']
                            },
                          }
                          },
                          "cdom_subtype": {
                          "title": "Subtype of condensation domain in this module",
                          "type": "string",
                          "required": true,
                          "enum":['Unknown', 'LCL', 'DCL', 'Dual', 'Starter', 'Heterocyclization', 'Other'],
                          "description": "Epimerization domains are not counted as C domains, but as a separate domain type."
                          },
                          "mod_doms": {
                          "title": "Scaffold-modifying domains in this module",
                          "type": "string",
                          "required": true,
                          "enum":['None', 'Oxidation', 'Methylation', 'Other'],
                          },
                          "other_mod_dom": {
                          "title": "Modifying domain type:",
                          "type": "string",
                          "dependencies": "mod_doms"
                          },
                          "mod_skip_iter": {
                          "title": "Module skipped or iterated during biosynthesis?",
                          "type": "string",
                          "required": true,
                          "enum":['Neither', 'Skipped', 'Iterated'],
                          },
                          "evidence_skip_iter": {
                          "title": "Evidence for skipping/iteration",
                          "type": "string",
                          "dependencies": "mod_skip_iter",
                          "required": true,
                          "enum":['Structure-based inference', 'Activity assay', 'Sequence-based prediction'],
                          },
                      },
                  },
                  }
              }    
          }
        },
        "te_type":{
          "title": "Thioesterase type:",
          "type": "string",
          "required": true,
          "enum":['Type I', 'Type II', 'Both', 'Unknown', 'None'],
          "description": "Also choose 'None' if it concerns a hybrid pathway and the thioesterase is an integral part of a PKS module at the end of the assembly-line."
        },
        "thioesterase":{
          "title": "Thioesterase-encoding gene(s):",
          "type": "string",
          "dependencies": "te_type",
          "description":"Enter locus tags, protein IDs or gene IDs (in this order of preference) exactly as they are specified in the submitted GenBank/ENA/DDBJ entry, separated by comma's.",
        },
        "release_type":{
          "title": "Release/cyclization type:",
          "type": "string",
          "required": true,
          "enum":['Macrolactamization', 'Macrolactonization', 'Hydrolysis', 'Other', 'Unknown'],
        },
        //"naa": { //NOT NEEDED ANYMORE? CAN BE DERIVED FROM ASSEMBLY-LINE ORDER IN NRPS GENES; OR STILL USE, BUT SEQUENCE AFTER MODIFICATIONS, E.G. EPIMERIZATIONS?
        //  "title": "NRP amino acid sequence",
        //  "type": "string",
        //  "enum": ['Cl-NMe-Tyr','bOH-Cl-Tyr','Ahp']
        //}
      }
    },
    "Polyketide" : {
      "title": "Polyketide-specific parameters",
      "type": "object",
      "dependencies": "biosyn_class",
      "properties": {
        "pk_subclass" : {
          "title": "Polyketide subclass",
          "type": "string",
          "required": true,
          "enum": ['Macrolide', 'Polyether', 'Polyene', 'Ansamycin', 'Enediyine', 'Anthracycline', 'Angucycline', 'Tetracycline', 'Tetracenomycin', 'Benzoisochromanequinone', 'Polyphenol', 'Chalcone', 'Stilbene', 'Aryl polyene', 'Other'],
          "description": "Also choose 'Other' if the polyketide is a hybrid of multiple polyketide types."
        },
        "lin_cycl_nrp": {
          "type": "string",
          "required": true,
          "enum": ['Linear', 'Cyclic'],
          "description": "Also counts as cyclic if the molecule is hybrid and the polyketide cyclizes onto another part of the hybrid molecule. Internal cyclizations of an amino acid or cyclizations of two adjacent amino acids or ketide groups do not count to define a cyclic compound: a cyclization should bridge residues that would otherwise not be connected."
        },
        "pks_subclass" : {
          "title": "Polyketide synthase subclass",
          "type": "string",
          "required": true,
          "default": "None",
          "enum": ['Modular type I', 'Iterative type I', 'Trans-AT type I', 'Enediyne type I', 'Type II', 'Type III', 'Other'],
          "description": "Hold ctrl or cmd key to select multiple items. The 'Modular Type I' option denotes the presence of cis-AT modules. If the pathway is a hybrid NRPS-PKS pathway, select the type of PKS responsible for the polyketide-synthesizing part of the pathway."
        },
        //"modular":{
        //  "description": "Includes trans-AT or hybrid NRPS-PKS pathways.",
        //  "type": "boolean",  //NOT NEEDED ANYMORE DUE TO IMPROVED DEPENDENCY
        //},
        "starter_unit" : {
          "title": "Starter unit",
          "type": "string",
          "dependencies": "pks_subclass",
          "required": true,
          "enum": ['Malonyl-CoA', 'Methylmalonyl-CoA', 'Methoxymalonyl-CoA', 'Ethylmalonyl-CoA', 'Hydroxymalonyl-CoA', 'Aminomalonyl-CoA', 'Malonamyl-CoA', 'Acetyl-CoA', 'Acetoacetyl-CoA', 'Propionyl-CoA', 'Benzoyl-CoA', 'Isobutyryl-CoA', '2-Methylbutyryl-CoA', '3-Methylbutyryl-CoA', 'Cyclohexylcarbonyl-CoA ', 'Trans-cyclopentane-(1R, 2R)-dicarboxylic acid', 'Mycolate', 'Glycolate', '3,5-AHBA-CoA', '3-Amino-2-Methylpropionate', 'p-Coumaroyl-CoA', 'p-Nitrobenzoate', 'p-Aminobenzoate', 'Butyryl-ACP', 'Hexanoyl-ACP', 'Other']
        },
        "mod_pks_genes": {
          "title": "Modular PKS genes in cluster",
          "dependencies":"pks_subclass",
          "type": "array",
          "items": {
            "type":"object",
              "title": "Modular PKS gene",
              "minItems":1,
              "properties":{
                  "_key":{
                   "title": "PKS-encoding gene:",
                   "description": "Modular polyketide synthase-encoding gene; Enter locus tag, protein ID or gene ID (in this order of preference) exactly as it is specified in the submitted GenBank/ENA/DDBJ entry.",
                   "type": "string"
                  },
                  "pks_module": {
                  "title": "PKS modules in this gene",
                  "type": "array",
                  "items": {
                      "type": "object",
                      "title": "PKS Module",
                      "properties": {
                          "module_nr": {
                          "title": "Assembly line module number",
                          "type": "string",
                          "pattern": "^[xABCD0123456789]{1,}$",
                          "description": "Enter the modules from N- to C-terminus in this gene.\n Counting starts from 1 at the start of the assembly-line (which can cover multiple genes), and includes loading modules (which are numbered '0'). Every module with at least an substrate-selecting domain (A / AT / CAL) is included in the count. If a module is split over two genes, the same number can be re-used for the domain(s) in the second gene. For hybrids, NRPS modules are included in the counting. If this PKS module is not part of a main assembly-line for producing this compound or if the PKS complex is too noncanonical to describe in a linear fashion, please enter 'x'. The same counts for monomodular precursor synthases such as MSAS synthase, which may be encoded within larger multi-modular PKS-encoding gene clusters. If there are multiple assembly-lines involved in synthesis of the main product, these can be indicated as 'A1, A2, A3, etc.' and 'B1, B2, B3'. Similarly, if the assembly-line branches at a later stage to make multiple products, the shared part can be called e.g. '1, 2, 3' and the split part 'A4, A5' and 'B4, B5'. Up to four parallel assembly-lines are supported (with letters A-D).",
                          "required": true,
                          },
                          "pks_domains": {
                          "title": "Core PKS domains present in this module",
                          "type": "string",
                          "required": true,
                          "enum":['Ketosynthase', 'Acyltransferase', 'Ketoreductase', 'Dehydratase', 'Enoylreductase', 'CoA-ligase'],
                          "description": "Hold ctrl or cmd key to select multiple items"
                          },
                          "at_substr_spec": {
                          "title": "AT/CAL domain substrate specificity",
                          "type": "string",
                          "default": "",
                          "dependencies": "pks_domains",
                          "description": "Enter 'None' if no AT-domain present (e.g., for trans-AT modules) or module is skipped. Enter 'Unknown' if specificity not known.",
                          "required": true,
                          "enum": ["None", "Unknown", "Malonyl-CoA", "Methylmalonyl-CoA", "Methoxymalonyl-CoA", "Ethylmalonyl-CoA", "Hydroxymalonyl-CoA", "Aminomalonyl-CoA", "Malonamyl-CoA", "Acetyl-CoA", "Acetoacetyl-CoA", "Propionyl-CoA", "Benzoyl-CoA", "Isobutyryl-CoA", "2-Methylbutyryl-CoA", "3-Methylbutyryl-CoA", "Cyclohexylcarbonyl-CoA ", "Trans-cyclopentane-(1R, 2R)-dicarboxylic acid", "Mycolate", "Glycolate", "3,5-AHBA-CoA", "3-Amino-2-Methylpropionate", "p-Coumaroyl-CoA", "p-Nitrobenzoate", "p-Aminobenzoate", "Butyryl-ACP", "Hexanoyl-ACP", "Other", "Multiple (promiscuous)"]
                          },
                          "multiple_spec" : {
                          "title": "Specify promiscuous keto group specificity",
                          "type": "string",
                          "dependencies": "at_substr_spec",
                          "description": "Input multiple keto groups, separated by semi-colons (';'). Use the exact same spelling as denoted in the multiple-choice option above."
                          },
                          "evidence_at_spec" : {
                          "title": "Evidence for AT/CAL specificity",
                          "description":"Choose 'None' if substrate specificity not known or not applicable.",
                          "type": "string",
                          "dependencies": "at_substr_spec",
                              "enum": ['Structure-based inference', 'Activity assay', 'Sequence-based prediction', 'Other']
                          },
                          "kr_stereochem": {
                          "title": "KR domain stereochemistry / activity",
                          "type": "string",
                          "dependencies":"pks_domains",
                          "required": true,
                          "enum":['Unknown', 'A-group', 'B-group', 'Inactive'],
                          "description": "A-group / B-group according to Caffrey, ChemBioChem 2003."
                          },
                          "mod_doms": {
                          "title": "Nonreductive scaffold-modifying domains in this module",
                          "description": "Any additional modifications made by domains within module",
                          "type": "string",
                          "required": true,
                          "enum":['None', 'Methylation', 'Pyran synthase', 'Michael branching', "Beta-branching", "Amination", "Other"],
                          },
                          "other_mod_dom": {
                          "title": "Modifying domain type:",
                          "type": "string",
                          "dependencies": "mod_doms"
                          },
                          "mod_skip_iter": {
                          "title": "Module skipped or iterated during biosynthesis?",
                          "type": "string",
                          "required": true,
                          "enum":['Neither', 'Skipped', 'Iterated'],
                          },
                          "evidence_skip_iter": {
                          "title": "Evidence for skipping/iteration",
                          "type": "string",
                          "dependencies": "mod_skip_iter",
                          "required": true,
                          "enum":['Structure-based inference', 'Activity assay', 'Sequence-based prediction'],
                          },
                      },
                  },
                  },
              }    
          }
        },
        "pks_genes":{
          "title": "Genes in cluster encoding polyketide synthases / ketosynthases",
          "type": "string",        
          "description": "Enter locus tags, protein IDs or gene IDs (in this order of preference), exactly as they are specified in the submitted GenBank/ENA/DDBJ entry, separated by comma's. For Type II PKSs, do not include chain length factor-encoding genes.",
          "dependencies": "pks_subclass",
        },
        "nr_iterations":{
          "title": "Number of chain synthesis iterations performed by iterative PKS module",
          "type": "number",
          "required": true,
          "dependencies": "pks_subclass",
        },
        "iterative_subtype":{
          "title": "Iterative PKS subtype",
          "type": "string",
          "required": true,
          "dependencies": "pks_subclass",
          "enum": ["Non-reducing", "Partially reducing", "Highly reducing"]
        }, // ADD cyclization type: “C2-C7 first” / “C4-C9 first” / “C6-C11 first” / “Other”
        "iter_cycl_type":{
          "title": "Iterative PKS cyclization type",
          "type": "string",
          "dependencies": "pks_subclass",
          "description": "Cyclization type according to Zhou, Li & Tang, Nat. Prod. Rep., 2010,27, 839-868.",
          "enum": ["C2-C7 first", "C4-C9 first", "C6-C11 first", "Other"]
        },
        "trans_at":{
          "title": "Genes in cluster encoding trans-acyltransferases",
          "type": "string",        
          "description": "Enter locus tags, protein IDs or gene IDs (in this order of preference), exactly as they are specified in the submitted GenBank/ENA/DDBJ entry, separated by comma's.",
          "dependencies": "pks_subclass",
        },
        "ketide_length":{
          "title": "Polyketide length",
          "type": "number",
          "required": true,
          "description": "Length of the ketide chain, e.g. 6 for a hexaketide or 9 for a nonaketide.",
          "dependencies": "pks_subclass",
        },
        "cyclases":{
          "title": "Gene(s) in cluster encoding polyketide cyclase(s) / aromatase(s)",
          "type": "string",        
          "description": "Enter locus tags, protein IDs or gene IDs (in this order of preference), exactly as they are specified in the submitted GenBank/ENA/DDBJ entry, separated by comma's.",
          "dependencies": "pks_subclass",
        },
        "te_type":{
          "title": "Thioesterase type:",
          "type": "string",
          "required": true,
          "dependencies": "pks_subclass",
          "enum":['Type I', 'Type II', 'Both', 'Unknown', 'None'],
          "description": "Also choose 'None' if it concerns a hybrid pathway and the thioesterase is an integral part of an NRPS module at the end of the assembly-line."
        },
        "thioesterase":{
          "title": "Thioesterase-encoding gene(s):",
          "dependencies": "te_type",
          "type": "string",
          "description":"Enter locus tags, protein IDs or gene IDs (in this order of preference) exactly as they are specified in the submitted GenBank/ENA/DDBJ entry, separated by comma's.",
        },
        "release_type":{
          "title": "Release/cyclization type:",
          "type": "string",
          "required": true,
          "dependencies": "pks_subclass",
          "enum":['Macrolactamization', 'Macrolactonization', 'Hydrolysis', 'Other', 'Unknown'],
        },
      }
    },
    "RiPP" : {
      "title": "RiPP-specific parameters",
      "type": "object",
      "dependencies": "biosyn_class",
      "properties": {
        "ripp_subclass" : {
          "title": "RiPP subclass",
          "required": true,
          "type": "string",
          "enum": ['lantipeptide', 'linaridin', 'proteusin', 'LAP', 'cyanobactin', 'thiopeptide', 'bottromycin', 'microcin', 'lasso peptide', 'microviridin', 'sactipeptide', 'head-to-tail cyclized peptide', 'amatoxin/phallotoxin', 'cyclotide', 'orbitide', 'conopeptide', 'glycocin', 'other']
        },
        "lin_cycl_ripp": {
          "type": "string",
          "required": true,
          "enum": ['Linear', 'Cyclic'],
          "description": "Internal cyclizations of an amino acid or cyclizations of two adjacent amino acids do not count to define a cyclic compound: a cyclization should bridge residues that would otherwise not be connected."
        },
        "precursor_loci": {
          "title": "Genes in cluster encoding RiPP precursor peptides",
          "type": "array",
          "items": {
            "type": "object",
            "title": "RiPP precursor",
            "properties": {
            "gene_id": {
              "title": "Gene ID:",
              "type": "string",        
              "minItems": 1,
              "description": "Enter locus tags, protein IDs or gene IDs (in this order of preference) that make up this operon, exactly as they are specified in the submitted GenBank/ENA/DDBJ entry, separated by comma's."
            },
            "core_pept_aa": {
              "title": "RiPP core peptide amino acid sequence",
              "description" : "Input sequence as single-AA abbreviations, e.g. 'ITSISLCTPGCKTGALMGCNMKTATCHCSIHVSK'",
              "type": "string",
            },
            "lead_pept_len": {
              "title": "Length of the leader peptide",
              "description" : "Length in AA, including N-terminal signal if present.",
              "type": "number",
            },
            "foll_pept_len": {
              "title": "Length of the follower peptide",
              "type": "number",
            },
            "cleavage_recogn_site": {
              "title": "Amino acid sequence of cleavage recognition site, if known",
              "type": "string",
            },
            "recogn_motif": {
              "title": "Recognition motif in leader peptide for modification peptide",
              "description": "E.g., FNLD for certain lanthipeptides.",
              "type": "string",
            },
            "peptidase": {
              "title": "Peptidase(s) involved in precursor cleavage",
              "description": "Enter locus tags, protein IDs or gene IDs (in this order of preference), exactly as they are used in the submitted GenBank/ENA/DDBJ entry, separated by comma's.",
              "type": "string",
            },
            "crosslinks": {
              "title": "Crosslinks within final peptide",
              "type": "array",
              "items": {
                "type":"object",
                "title":"Crosslink",
                "properties": {
                  "AA_pos_1": {
                    "title": "Position of first amino acid involved",
                    "type": "number"
                  },
                  "AA_pos_2": {
                    "title": "Position of second amino acid involved",
                    "type": "number"
                  },
                  "crosslink_type": {
                    "title": "Type of crosslink",
                    "type": "string",
                    "required": true,
                    "enum": ['Thioether', 'Ether', 'Lactam', 'Lactone', 'Other']
                  }
                }
              }
            }
            }
          }
        },
      }
    },
    "Terpene" : {
      "title": "Terpene-specific parameters",
      "type": "object",
      "dependencies": "biosyn_class",
      "properties": {
        "terpene_subclass" : {
          "title": "Terpene subclass, if applicable",
          "type": "string",
          "required": true,
          "enum": ['Sterol', 'Dolichol', 'Hopanoid', 'Quinone', 'Gibberellin', 'Carotenoid', 'Tocopherol', 'Other']
        },
        "terpene_c_len": {
          "title": "Terpene subclass by number of carbon units",
          "type": "string",
          "required": true,
          "enum": ['Hemiterpene', 'Monoterpene', 'Homoterpene (C11)', 'Sesquiterpene', 'Homoterpene (C16)', 'Diterpene', 'Sesterterpene', 'Triterpene', 'Sesquarterpene', 'Tetraterpene', 'Polyterpene', 'Norisoprenoid', 'Other']
        },
        "terpene_precursor": {
          "title": "Final isoprenoid precursor",
          "type": "string",
          "required": true,
          "enum": ['DMAPP', 'IPP', 'GPP', 'FPP', 'GGPP', 'GFPP', 'UPP', 'NPP', 'Other']
        },
        "terpene_synth_cycl": {
          "title": "Genes in cluster encoding terpene synthases/cyclases",
          "type": "string",        
          "description": "Enter locus tags, protein IDs or gene IDs (in this order of preference), exactly as they are specified in the submitted GenBank/ENA/DDBJ entry, separated by comma's.",
        },
        "prenyl_transf": {
          "title": "Genes in cluster encoding prenyltransferases",
          "type": "string",        
          "description": "Enter locus tags, protein IDs or gene IDs (in this order of preference), exactly as they are specified in the submitted GenBank/ENA/DDBJ entry, separated by comma's.",
        },
      }
    },
    "Saccharide" : {
      "title": "Saccharide-specific parameters",
      "type": "object",
      "dependencies": "biosyn_class",
      "properties": {
        "saccharide_subclass" : {
          "title": "Saccharide subclass",
          "type": "string",
          "required": true,
          "default": "None",
          "enum": ['hybrid/tailoring', 'oligosaccharide', 'aminoglycoside', 'aminocyclitol', 'lipopolysaccharide', 'exopolysaccharide', 'capsular polysaccharide', 'other']
        },
        "gt_genes": {
          "title": "Genes in cluster encoding glycosyl transferases",
          "type": "array",
          "items": {
              "title": "Glycosyl transferase-encoding gene",
              "type": "object",        
              "properties": {
                  "_key" : {
                  "title": "Gene ID",
                  "type": "string",
              "description": "Enter locus tag, protein ID or gene ID (in this order of preference), exactly as it is used in the submitted GenBank/ENA/DDBJ entry."
                  },
                  "gt_specificity" : {
                      "title": "Specificity of glycosyltransferase",
                      "type": "string",
                      "required": true,
                      "default":"None",
                      "enum": ["Unknown", "2-deoxy-L-fucose", "2-O-methyl-L-rhamnose", "2-thioglucose", "2,3-O-dimethyl-L-rhamnose", "2,3,4-tri-O-methylrhamnose", "2,4-O-dimethyl-L-rhamnose", "2'-N-methyl-D-fucosamine", "3-(5'-methyl-2'-pyrrolylcarbonyl-)4-O-methyl-L-noviose", "3-epi-L-vancosamine", "3-methoxy-D-mannose", "3-N-acetyl-4-O-acetyl-D-ravidosamine", "3-N-acetyl-D-ravidosamine", "3-N,N-dimethyl-D-mycosamine", "3-N,N-dimethyl-L-eremosamine", "3-O-carbamoyl-4-O-methyl-L-noviose", "3-O-carbamoyl-L-noviose", "3-O-methyl-L-olivose/L-oleandrose", "3-O-methyl-rhamnose", "3,4-O-dimethyl-L-rhamnose", "4-deoxy-4-methylthio-a-D-digitoxose ", "4-deoxy-4-thio-D-digitoxose ", "4-N-ethyl-4-amino-3-O-methoxy-2,4,5-trideoxypentose", "4-N,N-dimethylamino-4-deoxy-5-C-methyl-l-rhamnose", "4-O-acetyl-D-ravidosamine", "4-O-acetyl-L-arcanose", "4-O-carbamoyl-D-olivose", "4-O-methyl-L-rhodinose", "4-oxo-L-vancosamine", "4,6-dideoxy-4-hydroxylamino-D-glucose", "6-deoxy-3-C-methyl-L-mannose", "6-methoxy-D-glucose", "Chromose D (4-O-acetyl-β-D-oliose)", "D-3-N-methyl-4-O-methyl-L-ristosamine", "D-angolosamine", "D-arabinose", "D-chalcose", "D-desosamine", "D-digitalose", "D-digitoxose", "D-forosamine", "D-fucofuranose", "D-fucose", "D-galactose", "D-galacturonic acid", "D-glucosamine", "D-glucose", "D-mannose", "D-mycarose", "D-mycosamine", "D-oliose", "D-olivose ", "D-quinovose", "D-ravidosamine", "D-rhamnose", "Glucuronic acid", "Kedarosamine", "Ketodeoxyoctonic acid", "L-acosamine", "L-actinosamine", "L-aculose", "L-boivinose", "L-cinerulose A", "L-cinerulose B", "L-cladinose", "L-daunosamine ", "L-digitoxose", "L-fucose", "L-megosamine", "L-nogalose", "L-noviose", "L-quinovose", "L-rhamnose", "L-rhamnose", "L-rhodinose", "L-rhodosamine ", "L-ristosamine", "L-vancosamine", "L-vicenisamine", "N-5-acetylneuraminic acid", "N-acetyl-D-galactose", "N-acetyl-D-glucosamine", "N,N-dimethyl-L-pyrrolosamine", "Namenamicin sugar C", "Nogalamine", "O-methyl-L-amicetose", "Olivomose", "Olivomycose (L-chromose B)", "Rednose", "Other"]
                  },
                  "other_gt_spec" : {
                      "title": "Other specificity",
                      "dependencies": "gt_specificity",
                      "type": "string",
                  },
                  "evidence_gt_spec" : {
                      "title": "Evidence",
                      "dependencies": "gt_specificity",
                      //"description": "Choose 'None' if specificity is unknown",
                      "type": "string",
                      "enum": ['Structure-based inference', 'Knock-out', 'Activity assay', 'Sequence-based prediction', 'Other']
                  },
                  "sugar_subcluster" : {
                      "title": "Sub-cluster for sugar biosynthesis, if within this gene cluster",
                      "dependencies": "gt_specificity",
                      "description":"Enter locus tags, gene IDs or protein IDs, separated by comma's. A sub-cluster only needs to be declared once, in case there are multiple glycosyltransferases with the same substrate specificity.",
                      "type": "string",
                  }
              },
          }
        },
      }
    },
    "Alkaloid" : {
      "title": "Alkaloid-specific parameters",
      "type": "object",
      "dependencies": "biosyn_class",
      "properties": {
        "alkaloid_subclass" : {
          "title": "Alkaloid subclass",
          "required": true,
          "default": "None",
          "type": "string",
          "enum": ['pyrrole/pyrrolidine', 'pyrrolizidine', 'pyridine', 'piperidine', 'tropane', 'quinoline', 'isoquinoline', 'aporphine', 'quinolizidine', 'indole/benzopyrrole', 'indolizidine', 'imidazole', 'purine', 'steroidal', 'terpenoid', 'non-heterocyclic', 'other']
        }
      }
    },
    "Other" : { //SHOULD BE REQUIRED, BUT NOT WITH STAR BLURRING THE TEXT
      "title": "Parameters specific for other classes",
      "type": "object",
      "dependencies": "biosyn_class",
      "properties": {
        "other_subclass" : {
          "title": "Other biosynthetic class",
          "type": "string",
          "required": true,
          "default": "None",
          "enum": ['aminocoumarin', 'non-NRP beta-lactam', 'non-NRP siderophore', 'butyrolactone', 'ectoine', 'furan', 'homoserine lactone', 'melanin', 'nucleoside', 'phenazine', 'phosphoglycolipid', 'phosphonate', 'cyclitol', 'glucosinate', 'amino acid-derived', 'shikimate-derived', 'tRNA-derived', 'other']
        }
      }
    }
    
   }
   },
   "comments":{
     "type": "string",
     "title": "Please write any final comments here.",
    }
  }
};









/********   CONFIGURATION  ********/

var targetContainerId = "osd-sample-form"; 
var checkListFormAction = "TheAction";
     
var schemaLocation = "https://dl.dropboxusercontent.com/u/1611524/gcdj_widget/gcdj-osd-widget/js/data/osd_schema.json";  
var optionsLocation = "https://dl.dropboxusercontent.com/u/1611524/gcdj_widget/gcdj-osd-widget/js/data/osd_options.json";

// add extra validation if you like
function submitCheckListForm(form){ 
    return true;  
}  
  
/********   END CONFIGURATION  *****/

/*
* "dataSource": "https://dl.dropboxusercontent.com/u/1611524/gcdj_widget/gcdj-osd-widget/js/data/test.json",
*/
 
var inputBundle = {
    "options": mibig_options,
    "schema": mibig_schema,

    //create means no inital validation
"view": {
            "parent": "VIEW_JQUERYUI_CREATE",
            "wizard": {
                "renderWizard": true,
                "statusBar": true,
                "buttons": {
                    "done": {
                        "validateOnClick": true,
                        //"description":"Submit", //Doesn't work
                        "onClick": function() {
                          submit();
                          //alert("Fired done.onClick handler");
                        }
                    },
                    "next": {
                        "validateOnClick": true,
                        "onClick": function() {
                            //alert("Fired next.onClick handler");
                        }
                    },
                    "prev": {
                        "validateOnClick": true,
                        "onClick": function() {
                            //alert("Fired prev.onClick handler");
                        }
                    }
                },
                //"validation": true, //use wizard template instead to allow validation?
                "steps": 3,
                "bindings": {
                  "personal": 1,
                  "general_params": 2,
                  "comments": 3
                },
                "stepTitles": [{
                    "title": "Personal",
                    "description": "Name, E-mail, etc."
                }, {
                    "title": "MIBiG Data",
                    "description": "Annotation of the gene cluster"
                }, {
                    "title": "Comments",
                    "description": "Clarifications, comments, etc."
                }]
            }
        },
  "isDynamicCreation": true, 
  "postRender": function(field) {
     var form = field.form; 
       if (form) {
         form.refreshValidationState(false);
         form.registerSubmitHandler(function(e, form) {
           // validate the entire form (top control + all children)
           form.validate(true);
           // draw the validation state (top control + all children)
           form.refreshValidationState(true);
           // now display something
           if (form.isFormValid()) { 
             
             var json = form.getValue();
             console.log("json=" + JSON.stringify(json));   
             alert("The form looks good! \n\n" + json.sample.depth );
           } else {
              alert("Some fields are still missing or have incorrect input. The corresponding fields have been marked in red. Please go back and make the any necessary corrections.");
           } 
           e.stopPropagation();
           return false;
     });
   }     
   
  }
}; 

function renderForm() {
    $("#"+targetContainerId).alpaca(inputBundle);
}

$(document).ready(function() {
	Alpaca.defaultUI = "jquery-ui";
//Alpaca.logLevel = Alpaca.DEBUG;
    renderForm();
});